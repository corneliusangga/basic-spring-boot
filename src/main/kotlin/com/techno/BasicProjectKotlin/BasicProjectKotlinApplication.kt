package com.techno.BasicProjectKotlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BasicProjectKotlinApplication

fun main(args: Array<String>) {
	runApplication<BasicProjectKotlinApplication>(*args)
}
