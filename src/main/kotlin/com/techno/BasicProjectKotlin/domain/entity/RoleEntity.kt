package com.techno.BasicProjectKotlin.domain.entity

import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "mst_role")
data class RoleEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @field:Column(name = "id_role", columnDefinition = "UUID")
    val idRole : UUID? = null,

    @field:Column(name = "name", columnDefinition = "varchar")
    val name : String? = null
) : Serializable
