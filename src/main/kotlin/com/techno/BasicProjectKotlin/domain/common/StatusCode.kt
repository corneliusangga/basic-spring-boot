package com.techno.BasicProjectKotlin.domain.common

enum class StatusCode(val code : Boolean) {
    SUCCESS(true),
    FAILED(false)
}