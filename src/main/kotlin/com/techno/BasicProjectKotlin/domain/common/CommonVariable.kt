package com.techno.BasicProjectKotlin.domain.common

class CommonVariable {
    companion object{
        val SUCCESS_MESSAGE = "Success"
        val FAILED_MESSAGE = "Something went wrong"
    }
}