package com.techno.BasicProjectKotlin.domain.dto.request

import org.jetbrains.annotations.NotNull
import javax.validation.constraints.Negative
import javax.validation.constraints.NegativeOrZero
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Pattern
import javax.validation.constraints.Positive
import javax.validation.constraints.PositiveOrZero
import javax.validation.constraints.Size

data class ReqDataDto (
    @field:NotBlank(message = "NotBlank")
    val notNullRequest: String?,

    @field:NotEmpty(message = "NotEmpty")
    val notEmptyRequest: String?,

    @field:NotNull
    @field:NotEmpty(message = "Not Empty")
    @field:Size(min = 5, max = 10)
    val minMaxRequest: String?,

    @field:Positive(message = "Positive")
    val positiveRequest: Int?,

    @field:PositiveOrZero(message = "Positive Or Zero")
    val positiveOrZeroRequest: Int?,

    @field:Negative(message = "Negative")
    val negativeRequest: Int?,

    @field:NegativeOrZero(message = "Negative Or Zero")
    val negativeOrZeroRequest: Int?,

    @field:NotNull
    @field:NotEmpty(message = "Not Empty")
    @field:Pattern(regexp = "^[a-zA-z]*$", message = "pattern not valid")
    val patternRequest: String?
)