package com.techno.BasicProjectKotlin.domain.dto.response

class ResEncodeJWT (
    val id: Int = 0,
    val token: String = ""
)