package com.techno.BasicProjectKotlin.domain.dto.response

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.UUID

data class ResUserDto(
    @field:JsonProperty(value = "id_user")
    val idUser : String? = null,

    @field:JsonProperty(value = "first_name")
    val firstName : String? = null,

    @field:JsonProperty(value = "last_name")
    val lastName : String? = null,

    @field:JsonProperty(value = "age")
    val age: Int? = null,

    @field:JsonProperty(value = "email")
    val email: String? = null,

    @field:JsonProperty(value = "role")
    val role: String? = null
)
