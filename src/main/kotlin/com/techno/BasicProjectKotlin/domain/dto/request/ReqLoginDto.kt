package com.techno.BasicProjectKotlin.domain.dto.request

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.annotations.NotNull
import javax.validation.constraints.NotEmpty

data class ReqLoginDto(
    @field:NotNull
    @field:NotEmpty(message = "Not Empty")
    @field:JsonProperty(value = "email")
    val email: String,

    @field:NotNull
    @field:NotEmpty(message = "Not Empty")
    @field:JsonProperty(value = "password")
    val password : String
)
