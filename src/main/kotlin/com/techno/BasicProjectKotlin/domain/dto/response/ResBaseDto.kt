package com.techno.BasicProjectKotlin.domain.dto.response

import com.fasterxml.jackson.annotation.JsonProperty
import com.techno.BasicProjectKotlin.domain.common.CommonVariable
import com.techno.BasicProjectKotlin.domain.common.StatusCode

class ResBaseDto<T>(
    @field:JsonProperty(value = "OUT_STAT")
    val outStat: Boolean = StatusCode.SUCCESS.code,

    @field:JsonProperty(value = "OUT_MESS")
    val outMess: String = CommonVariable.SUCCESS_MESSAGE,

    @field:JsonProperty(value = "OUT_DATA")
    val data : T? = null
)