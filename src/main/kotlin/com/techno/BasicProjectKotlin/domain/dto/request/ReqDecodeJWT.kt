package com.techno.BasicProjectKotlin.domain.dto.request

data class ReqDecodeJWT(
    val token : String = ""
)
