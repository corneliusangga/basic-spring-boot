package com.techno.BasicProjectKotlin.domain.dto.response

data class ResDecodeJWT (
    val id : Int = 0,
    val email : String = ""
)