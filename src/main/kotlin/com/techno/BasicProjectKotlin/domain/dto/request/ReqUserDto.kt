package com.techno.BasicProjectKotlin.domain.dto.request

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

data class ReqUserDto(
    @field:JsonProperty(value = "first_name")
    val firstName : String? = null,

    @field:JsonProperty(value = "last_name")
    val lastName : String? = null,

    @field:JsonProperty(value = "password")
    val password : String? = null,

    @field:JsonProperty(value = "age")
    val age: Int? = null,

    @field:JsonProperty(value = "email")
    val email: String? = null,

    @field:JsonProperty(value = "role")
    val role: String? = null
)