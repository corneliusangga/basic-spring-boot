package com.techno.BasicProjectKotlin.repository

import com.techno.BasicProjectKotlin.domain.entity.UserEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.transaction.annotation.Transactional
import java.util.UUID

interface UserRepository : JpaRepository<UserEntity, String> {
    //Query by method
    fun findByIdUser(idUser : UUID) : UserEntity?

    fun findByEmailAndPassword(email : String, password : String) : UserEntity?

    //Query Raw
    @Query("SELECT a FROM UserEntity a WHERE idUser = :idUser")
    fun getByIdUser(idUser : UUID) : UserEntity?

    //Query Native
    @Query("SELECT * FROM mst_user WHERE idUser = :idUser", nativeQuery = true)
    fun getByIdUserNative(idUser : UUID) : UserEntity?

    //Notasi dipakai untuk method Create, Update, Delete
    @Modifying
    @Transactional
    fun deleteByIdUser(idUser : UUID) : Int?
}