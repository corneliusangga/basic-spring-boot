package com.techno.BasicProjectKotlin.service.impl

import com.techno.BasicProjectKotlin.domain.common.CommonVariable
import com.techno.BasicProjectKotlin.domain.common.StatusCode
import com.techno.BasicProjectKotlin.domain.dto.request.ReqUserDto
import com.techno.BasicProjectKotlin.domain.dto.response.ResBaseDto
import com.techno.BasicProjectKotlin.domain.dto.response.ResUserDto
import com.techno.BasicProjectKotlin.domain.entity.UserEntity
import com.techno.BasicProjectKotlin.repository.UserRepository
import com.techno.BasicProjectKotlin.service.UserService
import org.springframework.stereotype.Service
import java.util.*
import kotlin.collections.ArrayList

@Service
class UserServiceImpl(
    private val userRepository: UserRepository
) : UserService {
    override fun getUserAll(): ResBaseDto<ArrayList<ResUserDto>> {
        val data = userRepository.findAll()
        val response : ArrayList<ResUserDto> = ArrayList()

        data.forEach{
            response.add(
                ResUserDto(
                    idUser = it.idUser.toString(),
                    firstName = it.firstName,
                    lastName = it.lastName,
                    age = it.age,
                    email = it.email,
                    role = it.idRole?.name
                )
            )
        }

        return ResBaseDto(data = response)
    }

    override fun getUserById(idUser: String): ResBaseDto<ResUserDto> {
        val data = userRepository.findByIdUser(UUID.fromString(idUser))

        if(data == null){
            return ResBaseDto(
                outStat = StatusCode.FAILED.code,
                outMess = CommonVariable.FAILED_MESSAGE,
                data = null
            )
        }

        val response = ResUserDto(
            idUser = data.idUser.toString(),
            firstName = data.firstName,
            lastName = data.lastName,
            age = data.age,
            email = data.email,
            role = data.role
        )

        return ResBaseDto(data = response)
    }

    override fun getUserbyEmailAndPassword(email : String, password : String) : ResBaseDto<ResUserDto> {
        val data = userRepository.findByEmailAndPassword(email, password)

        if(data == null){
            return ResBaseDto(
                outStat = StatusCode.FAILED.code,
                outMess = CommonVariable.FAILED_MESSAGE,
                data = null
            )
        }

        val response = ResUserDto(
            idUser = data.idUser.toString(),
            firstName = data.firstName,
            lastName = data.lastName,
            age = data.age,
            email = data.email,
            role = data.idRole?.name
        )

        return ResBaseDto(
            outStat = StatusCode.SUCCESS.code,
            outMess = CommonVariable.SUCCESS_MESSAGE,
            data = response
        )
    }

    override fun insertUser(reqUserDto: ReqUserDto): ResBaseDto<ResUserDto> {
        val data = UserEntity(
            firstName = reqUserDto.firstName,
            lastName = reqUserDto.lastName,
            age = reqUserDto.age,
            email = reqUserDto.email,
            password = reqUserDto.password,
            role = reqUserDto.role
        )

        val resUser = userRepository.save(data)
        val response = ResUserDto(
            idUser = resUser.idUser.toString(),
            firstName = resUser.firstName,
            lastName = resUser.lastName,
            age = resUser.age,
            email = resUser.email,
            role = resUser.role
        )

        return ResBaseDto(data = response)
    }

    override fun updateUser(reqUserDto: ReqUserDto, idUser: String): ResBaseDto<ResUserDto> {
        val data = userRepository.findByIdUser(UUID.fromString(idUser)) ?:
            return ResBaseDto(
                outStat = StatusCode.FAILED.code,
                outMess = CommonVariable.FAILED_MESSAGE,
                data = null
            )
        val newData = data.copy(
            firstName = reqUserDto.firstName,
            lastName = reqUserDto.lastName,
            age = reqUserDto.age,
            email = reqUserDto.email,
            password = reqUserDto.password,
            role = reqUserDto.role
        )
        val resUser = userRepository.save(newData)
        val response = ResUserDto(
            idUser = resUser.idUser.toString(),
            firstName = resUser.firstName,
            lastName = resUser.lastName,
            age = resUser.age,
            email = resUser.email,
            role = resUser.role
        )

        return ResBaseDto(data = response)
    }

    override fun deleteUser(idUser: String): ResBaseDto<Any> {
        userRepository.deleteByIdUser(UUID.fromString(idUser)) ?:
            return ResBaseDto(
                outStat = StatusCode.FAILED.code,
                outMess = CommonVariable.FAILED_MESSAGE,
                data = null
            )

        return ResBaseDto(data = null)
    }

}