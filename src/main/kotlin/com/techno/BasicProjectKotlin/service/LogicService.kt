package com.techno.BasicProjectKotlin.service

interface LogicService {
    fun oddOrEven(number: Int) : String
}