package com.techno.BasicProjectKotlin.service

import com.techno.BasicProjectKotlin.domain.dto.request.ReqUserDto
import com.techno.BasicProjectKotlin.domain.dto.response.ResBaseDto
import com.techno.BasicProjectKotlin.domain.dto.response.ResUserDto
import java.util.UUID

interface UserService {
    fun getUserAll() : ResBaseDto<ArrayList<ResUserDto>>
    fun getUserById(idUser : String) : ResBaseDto<ResUserDto>
    fun getUserbyEmailAndPassword(email : String, password : String) : ResBaseDto<ResUserDto>
    fun insertUser(reqUserDto: ReqUserDto) : ResBaseDto<ResUserDto>
    fun updateUser(reqUserDto: ReqUserDto, idUser: String) : ResBaseDto<ResUserDto>
    fun deleteUser(idUser : String) : ResBaseDto<Any>
}