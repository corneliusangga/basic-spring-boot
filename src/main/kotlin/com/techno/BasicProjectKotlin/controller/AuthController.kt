package com.techno.BasicProjectKotlin.controller

import com.techno.BasicProjectKotlin.domain.dto.request.ReqLoginDto
import com.techno.BasicProjectKotlin.domain.dto.response.ResBaseDto
import com.techno.BasicProjectKotlin.service.UserService
import com.techno.BasicProjectKotlin.util.JWTGenerator
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
@RequestMapping("/v1/api/auth")
class AuthController(
    private val userService: UserService
) {
    @PostMapping("/login")
    fun insertUser(@Valid @RequestBody reqLoginDto: ReqLoginDto): ResponseEntity<ResBaseDto<String>> {
        val user = userService.getUserbyEmailAndPassword(reqLoginDto.email, reqLoginDto.password)

        if(user.data?.idUser == null || user.data.email == null){
            return ResponseEntity.ok(ResBaseDto(
                outStat = false,
                outMess = "User not found, failed to get JWToken",
                data = null
            ))
        }

        val token = JWTGenerator().createJWT(
            user.data.idUser,
            user.data.email
        )

        return ResponseEntity.ok(ResBaseDto(
            outStat = true,
            outMess = "Success get JWToken",
            data = token))
    }
}