package com.techno.BasicProjectKotlin.controller

import com.techno.BasicProjectKotlin.domain.dto.request.ReqUserDto
import com.techno.BasicProjectKotlin.domain.dto.response.ResBaseDto
import com.techno.BasicProjectKotlin.domain.dto.response.ResUserDto
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/v1/api")
class TestController {
    val firstName: String = "Cornelius"
    val lastName : String = "Angga"

    @GetMapping("/test")
    fun testGetMapping(): ResponseEntity<Any>{
        val response: LinkedHashMap<String, String> = LinkedHashMap()
        response["firstName"] = firstName
        response["lastName"] = lastName

        return ResponseEntity.ok().body(response)
    }

    @GetMapping("/user")
    fun getUser(@RequestParam("age") age : String): ResponseEntity<Any>{
        val response: LinkedHashMap<String, String> = LinkedHashMap()
        response["firstName"] = firstName
        response["lastName"] = lastName
        response["age"] = age

        return ResponseEntity.ok().body(response)
    }

    @GetMapping("/user/{age}")
    fun getUserByAge(@PathVariable("age") age : String): ResponseEntity<Any>{
        val response: LinkedHashMap<String, String> = LinkedHashMap()
        response["firstName"] = firstName
        response["lastName"] = lastName
        response["age"] = age

        return ResponseEntity.ok().body(response)
    }

    @GetMapping("/user/dto")
    fun getUserDto(@RequestParam("age") age : Int): ResponseEntity<ResUserDto>{
        val response = ResUserDto(
            firstName = this.firstName,
            lastName = this.lastName,
            age = age
        )

        return ResponseEntity.ok().body(response)
    }

    @PostMapping("/user")
    fun postUser(@RequestBody reqUserDto: ReqUserDto): ResponseEntity<ResBaseDto<ResUserDto>>{
        println("Incomming request $reqUserDto")

        val data = ResUserDto(
            firstName = reqUserDto.firstName,
            lastName = reqUserDto.lastName,
            age = reqUserDto.age
        )

        val response = ResBaseDto(data = data)

        return ResponseEntity.ok().body(response)
    }
}
