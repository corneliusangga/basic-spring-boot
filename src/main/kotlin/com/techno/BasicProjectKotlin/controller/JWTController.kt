package com.techno.BasicProjectKotlin.controller

import com.techno.BasicProjectKotlin.domain.dto.request.ReqDecodeJWT
import com.techno.BasicProjectKotlin.domain.dto.request.ReqEncodeJWT
import com.techno.BasicProjectKotlin.domain.dto.response.ResBaseDto
import com.techno.BasicProjectKotlin.domain.dto.response.ResDecodeJWT
import com.techno.BasicProjectKotlin.domain.dto.response.ResEncodeJWT
import com.techno.BasicProjectKotlin.util.JWTGenerator
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/v1/api/jwt")
class JWTController {
    @PostMapping("/encode")
    fun encodeJWT(@RequestBody reqEncodeJWT: ReqEncodeJWT): ResponseEntity<ResBaseDto<ResEncodeJWT>> {
        val token = JWTGenerator().createJWT(
            reqEncodeJWT.id.toString(),
            reqEncodeJWT.email
        )

        return ResponseEntity.ok(ResBaseDto(
            outStat = true,
            outMess = "Success Get Token JWT",
            data = ResEncodeJWT(reqEncodeJWT.id, token)))
    }

    @PostMapping("/decode")
    fun decodeJWT(@RequestBody request: ReqDecodeJWT): ResponseEntity<ResBaseDto<ResDecodeJWT>>{
        val claim = JWTGenerator().decodeJWT(request.token)

        return ResponseEntity.ok(ResBaseDto(
            outStat = true,
            outMess = "Success Decode JWT",
            data = ResDecodeJWT(Integer.valueOf(claim.id), claim.subject)
        ))
    }
}