package com.techno.BasicProjectKotlin.config

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
class PathMatchingInterceptor(
    val requestInterceptor: RequestInterceptor,
    val jwtokenInterceptor: JWTokenInterceptor
) : WebMvcConfigurer {
    override fun addInterceptors(registery : InterceptorRegistry){
        registery.addInterceptor(requestInterceptor).excludePathPatterns(
            "/swagger-ui/**",
            "/v2/api-docs",
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/webjars/**"
        )
        registery.addInterceptor(jwtokenInterceptor).excludePathPatterns(
            "/v1/api/auth/**",
            "/swagger-ui/**",
            "/v2/api-docs",
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/webjars/**"
        )

    }
}