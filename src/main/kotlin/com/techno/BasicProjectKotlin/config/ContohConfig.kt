package com.techno.BasicProjectKotlin.config

import com.techno.BasicProjectKotlin.service.LogicService
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component

@Component
class ContohConfig (
    private val logicService: LogicService
){
    @Bean
    fun printName(){
        println("Hello my name is angga")
    }

    @Bean
    fun printOddOrEven(){
        println("Nilai 5 = ${logicService.oddOrEven(5)}")
    }
}