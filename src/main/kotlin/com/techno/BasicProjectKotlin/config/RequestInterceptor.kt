package com.techno.BasicProjectKotlin.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.techno.BasicProjectKotlin.domain.common.StatusCode
import com.techno.BasicProjectKotlin.domain.dto.response.ResBaseDto
import com.techno.BasicProjectKotlin.domain.dto.response.ResDecodeJWT
import com.techno.BasicProjectKotlin.util.JWTGenerator
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.web.servlet.HandlerInterceptor
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class RequestInterceptor(
    @Value("\${header.request.api-key}")
    private val apiKey : String
) : HandlerInterceptor {
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        val apiKeyRequest = request.getHeader("API-Key")

        if(apiKeyRequest != apiKey){
            val body : ResBaseDto<String> = ResBaseDto(
                outStat = StatusCode.FAILED.code,
                outMess = "API Key Failed",
                data = null
            )

            internalServerError(body, response)
            return false
        }

        return super.preHandle(request, response, handler)
    }

    fun internalServerError(body : ResBaseDto<String>, response : HttpServletResponse) : HttpServletResponse{
        response.status = HttpStatus.FORBIDDEN.value()
        response.contentType = "application/json"
        response.writer.write(convertObjectToJson(body))

        return response
    }

    fun convertObjectToJson(dto : ResBaseDto<String>) : String? {
        return ObjectMapper().writeValueAsString(dto)
    }
}