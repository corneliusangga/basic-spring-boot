package com.techno.BasicProjectKotlin.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.techno.BasicProjectKotlin.domain.common.StatusCode
import com.techno.BasicProjectKotlin.domain.dto.response.ResBaseDto
import com.techno.BasicProjectKotlin.util.JWTGenerator
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.servlet.HandlerInterceptor
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class JWTokenInterceptor() : HandlerInterceptor {
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        val tokenRequest = request.getHeader("Token")

        if(!tokenRequest.isNullOrEmpty() && validateToken(tokenRequest)){
            return super.preHandle(request, response, handler)
        }

        val body: ResBaseDto<String> = ResBaseDto(
            outStat = StatusCode.FAILED.code,
            outMess = "JWToken Failed",
            data = null
        )

        internalServerError(body, response)
        return false

    }

    fun validateToken(token : String) : Boolean {
        val claim = JWTGenerator().decodeJWT(token)

        return claim.isNotEmpty()
    }

    fun internalServerError(body : ResBaseDto<String>, response : HttpServletResponse) : HttpServletResponse {
        response.status = HttpStatus.FORBIDDEN.value()
        response.contentType = "application/json"
        response.writer.write(convertObjectToJson(body))

        return response
    }

    fun convertObjectToJson(dto : ResBaseDto<String>) : String? {
        return ObjectMapper().writeValueAsString(dto)
    }
}